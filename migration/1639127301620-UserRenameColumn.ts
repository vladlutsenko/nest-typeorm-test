import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserRenameColumn1639127301620 implements MigrationInterface {
  name = 'UserRenameColumn1639127301620';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "user" RENAME COLUMN "testField" TO "testFieldRenamed"`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "user" RENAME COLUMN "testFieldRenamed" TO "testField"`,
    );
  }
}
