import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserAddColumn1639127250825 implements MigrationInterface {
  name = 'UserAddColumn1639127250825';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "user" ADD "testField" character varying NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "testField"`);
  }
}
