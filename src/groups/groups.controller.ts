import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { GroupsService } from './groups.service';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { AddUserToGroupGroupDto } from './dto/add-user-to-group.dto';

@Controller('groups')
export class GroupsController {
  constructor(private readonly groupsService: GroupsService) {}

  @Post()
  create(@Body() createGroupDto: CreateGroupDto) {
    return this.groupsService.create(createGroupDto);
  }

  @Post('add-user/:id')
  addUserToGroup(
    @Param('id') id: string,
    @Body() addUserToGroupDto: AddUserToGroupGroupDto,
  ) {
    return this.groupsService.addUserToGroup(+id, addUserToGroupDto.userId);
  }

  @Post('delete-user/:id')
  deleteUserFromGroup(
    @Param('id') id: string,
    @Body() deleteUserFromGroupDto: AddUserToGroupGroupDto,
  ) {
    return this.groupsService.deleteUserFromGroup(
      +id,
      deleteUserFromGroupDto.userId,
    );
  }

  @Get()
  findAll() {
    return this.groupsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.groupsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateGroupDto: UpdateGroupDto) {
    return this.groupsService.update(+id, updateGroupDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.groupsService.remove(+id);
  }
}
