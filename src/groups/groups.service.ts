import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../users/user.entity';
import { UsersService } from '../users/users.service';
import { Repository } from 'typeorm';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { Group } from './group.entity';

@Injectable()
export class GroupsService {
  constructor(
    @InjectRepository(Group)
    private groupsRepository: Repository<Group>,

    @InjectRepository(User)
    private usersRepository: Repository<User>,

    private readonly usersService: UsersService,
  ) {}

  create(createGroupDto: CreateGroupDto) {
    const group = this.groupsRepository.create(createGroupDto);
    return this.groupsRepository.save(group);
  }

  findAll() {
    return this.groupsRepository.find({ relations: ['users'] });
  }

  async findOne(id: number) {
    const group = await this.groupsRepository.findOne(
      { id: id },
      { relations: ['users'] },
    );
    if (group) {
      return group;
    }
    throw new HttpException('No group found', HttpStatus.NOT_FOUND);
  }

  async update(id: number, updateGroupDto: UpdateGroupDto) {
    const group = await this.findOne(id);
    return this.groupsRepository.save({ ...group, ...updateGroupDto });
  }

  remove(id: number) {
    return this.groupsRepository.delete(id);
  }

  async addUserToGroup(groupId: number, userId: number) {
    const user = await this.usersService.findOne(userId);
    const group = await this.findOne(groupId);

    group.users = [...group.users, user];

    return this.groupsRepository.save(group);
  }

  async deleteUserFromGroup(groupId: number, userId: number) {
    await this.usersService.findOne(userId);
    const group = await this.findOne(groupId);

    group.users = [...group.users.filter((user) => user.id !== userId)];

    return this.groupsRepository.save(group);
  }
}
