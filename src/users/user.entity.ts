import { Group } from '../groups/group.entity';
import { Entity, Column, PrimaryGeneratedColumn, ManyToMany } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ default: true })
  isActive: boolean;

  @Column()
  testFieldRenamed: string;

  @ManyToMany((type) => Group, (group) => group.users)
  groups: Group[];
}
