FROM node:14

WORKDIR /app

COPY package.json .

RUN npm i
RUN npm install -g ts-node

COPY . .

CMD npm run start:dev
